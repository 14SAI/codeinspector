def lambda_handler(event, context):
    print("Pipeline Test Function Invoked")
    response = {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
        },
        "statusCode": 200,
        "body": "PipelineTest Executed Successfully"
    }

    return response

# lambda_handler(None, None)
